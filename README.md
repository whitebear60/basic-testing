# Basic testing app

## Overview
Created as an educational project, this app lets you perform tests, that you (or your teacher) has sent to you.

Currently, no test designer is implemented, so you should write tests in *.json files manually.

You can [view the app](https://whitebear60.gitlab.io/basic-testing/) on GitLab Pages

## JSON file structure
The test file needs to be formatted as follows in order to work:

**name**: The name of the test<br />
**author**: The author of the test<br/>
**timeLimit**: The time limit (in seconds), after which the test will be stopped<br/>
**questions**: The questions of the test itself. It is an array (list of data of the same type) that consists of the objects (list of key-value pairs), formatted like this:

{<br>
**question**: The question title,<br>
**answers**: The array (list) of the answers. Currently only "choose-one-of-four" questions supported<br>
**correct**: The number that indicates the correct answer. Set 1 for the first answer to be correct, 2 for the second and so on.
<br>
}

    See test.json that is included in this repository for the example