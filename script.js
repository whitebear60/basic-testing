let testDef = {
    name: undefined,
    author: undefined,
    questions: [
        {
            question: undefined,
            answers: [undefined],
            correct: 0
        }
    ]
};

let correctAnswerCount = 0;
let wrongAnswerCount = 0;
let clockInterval;


document.addEventListener("DOMContentLoaded", () => {
    let fileInput = document.querySelector("#choose-test");
    fileInput.addEventListener("input", () => {
        if (fileInput.files.length > 0) {
            // console.log(fileInput.files[0].name)
            document.querySelector("#start-test").disabled = false;
            document.querySelector('label[for="choose-test"]').innerText = `Обраний файл: ${fileInput.files[0].name}`
        } else if (fileInput.files.length === 0) {
            document.querySelector("#start-test").disabled = true;
        }
    })
    document.querySelector("#choose-test-form").addEventListener("submit", (e) => {
        e.preventDefault();
        let fileInput = e.target.elements[0];
        if (fileInput.files.length > 0) {
            fileInput.files[0].text().then((value) => {
                testDef = JSON.parse(value.toString());
                loadTest(testDef);
            });
        }

    })
})

let loadTest = (testDef) => {
    let card = document.querySelector("#main");
    document.querySelector("#question").classList.remove("hidden");
    document.title = `Тестувальник - ${testDef.name}`
    document.querySelector("#app-title").innerText = `Тестувальник - ${testDef.name}`
    if (testDef.author) {
        let subtitle = document.createElement("h2");
        subtitle.innerText = `автор: ${testDef.author}`;
        subtitle.className = "subtitle"
        // console.log(subtitle)
        document.querySelector("#app-title").insertAdjacentElement("afterend", subtitle)
    }
    document.querySelector('#choose-test-form').classList.add("hidden");

    let timer = document.createElement("span");
    const timerEnd = Date.now()/1000 + parseInt(testDef.timeLimit);
    clockInterval = setInterval(clock, 1000, timer, timerEnd);
    timer.innerText = updateTimer(parseInt(testDef.timeLimit))



    let progressLabelMin = document.createElement('label');
    progressLabelMin.innerText = "0";
    progressLabelMin.htmlFor = "test-progress"
    progressLabelMin.id = "test-progress-label-min"

    let progressLabelMax = document.createElement('label');
    progressLabelMax.innerText = testDef.questions.length.toString();
    progressLabelMax.htmlFor = "test-progress"
    progressLabelMax.id = "test-progress-label-max"

    let progress = document.createElement("progress");
    progress.id = "test-progress"
    progress.max = testDef.questions.length;
    progress.value = 0;
    progress.title = `${progress.value} / ${progress.max}`

    let correctAnswers = document.createElement("span");
    correctAnswers.id = "test-correct-answer-count";
    correctAnswers.innerText = correctAnswerCount.toString();

    let wrongAnswers = document.createElement("span");
    wrongAnswers.id = "test-wrong-answer-count";
    wrongAnswers.innerText = wrongAnswerCount.toString();

    const div = document.createElement("div");
    div.id = "progress-wrap"

    div.appendChild(timer);
    div.appendChild(progressLabelMin);
    div.appendChild(progress);
    div.appendChild(progressLabelMax);
    div.appendChild(correctAnswers);
    div.appendChild(wrongAnswers);
    card.appendChild(div);
    let currentProgress = 0;
    updateProgress(currentProgress, correctAnswers, wrongAnswers);

    let selectedBtn = 1;
    showQuestion(testDef.questions[currentProgress]);
    document.querySelectorAll('input[type=radio][name=quiz]').forEach((el) => {
        el.addEventListener('click', () => {
            selectedBtn = parseInt(el.id.substring(6));
        })
    })
    document.questionForm.addEventListener('submit', (ev) => {
        ev.preventDefault();
        // console.log(testDef.questions[currentProgress])
        // console.log(selectedBtn)
        // console.log(testDef.questions[currentProgress].correct)
        selectedBtn === testDef.questions[currentProgress].correct ? ++correctAnswerCount : ++wrongAnswerCount;
        currentProgress++;

        document.questionForm.reset();
        updateProgress(currentProgress, correctAnswers, wrongAnswers)
        currentProgress < testDef.questions.length ? showQuestion(testDef.questions[currentProgress]) : end();
    })
}


let updateProgress = (questionsAnswered, correctAnswerElem, wrongAnswerElem) => {
    const progress = document.querySelector("#test-progress");
    const leftLabel = document.querySelector("#test-progress-label-min");
    const rightLabel = document.querySelector("#test-progress-label-max");
    progress.value = questionsAnswered;
    leftLabel.innerText = questionsAnswered;
    progress.title = `${questionsAnswered} / ${rightLabel.innerText}`
    correctAnswerElem.innerText = correctAnswerCount;
    wrongAnswerElem.innerText = wrongAnswerCount;
}

let end = () => {
    document.questionForm.classList.add('hidden');
    document.querySelector('#question-title').innerText = "Тест закінчився"
    clearInterval(clockInterval)
}

let clock = (timer, timerEnd) => {

    const remaining = Math.round(timerEnd - Date.now()/1000);
    timer.innerText = `${updateTimer(remaining)}`;
}

let updateTimer = (clock) => {
    let mins = Math.floor(clock/60);
    let secs = Math.floor(clock%60);
    mins < 10 ? mins = "0" + mins.toString() : null;
    secs < 10 ? secs = "0" + secs.toString() : null;
    if (clock === 0) end();
    return `${mins}:${secs}`;
}

let showQuestion = (question) => {
    let title = document.querySelector('#question-title');
    let answer1 = document.querySelector('label[for=answer1]');
    let answer2 = document.querySelector('label[for=answer2]');
    let answer3 = document.querySelector('label[for=answer3]');
    let answer4 = document.querySelector('label[for=answer4]');
    title.innerText = question.question;
    answer1.innerText = question.answers[0];
    answer2.innerText = question.answers[1];
    answer3.innerText = question.answers[2];
    answer4.innerText = question.answers[3];

}